const Sonya = require('./classes/Sonya')
	, Kitana = require('./classes/Kitana')
	, Raiden = require('./classes/Raiden')
	, Character = require('./classes/Character');

console.log('Context loaded!');

module.exports = () => ({
	Sonya: new Sonya({
		id: 'Sonya',
		bdlname: 'Соня',

		attributes: [
			'relationship',
			'depravity'
		],
		relationship: -25,
		depravity: 0,
		pain: 0,
		pleasure: 0,
		slots: [
			// 'underwear_bot',
			'outerwear_bot',
			'outerwear_top',
			'shoes',
			'emotion',
			'hairstyle',
		],
		clothes: [
			'outerwear_bot',
			'outerwear_top',
			'shoes',
		],
		// default: {
			// emotion: null,
			// hairstyle: 'Hair_short',
		// },
		already_called: false,
		first_meet: false,
		talk_about_boots: false,
		talk_about_hair: false,
		talk_about_Kitana: false,
		talk_about_short_top: false,
		talk_about_bridges: false,
		talk_about_shorts: false,
		talk_about_panties: false,
		talk_about_tits: false,
		wear_shorthair: false,
		wear_armyboots: false,
		wear_shorttop: false,
		wear_bridges: false,
		wear_shorts: false,
		wear_panties: false,
		start_learning: false,
		showing_panties: false,
		showing_tits: false,
		blowjob: false,
		final_blowjob: false
	}),
	Kitana: new Kitana({
		id: 'Kitana',
		bdlname: 'Китана',

		attributes: [
			'domination'
		],
		domination: 0,
		slots: [
			'hairstyle',
			'bottom',
			'gloves',
			'pants',
			'shoes',
			'top',
			'body',
		],
		clothes: [
			'bottom',
			'gloves',
			'pants',
			'top'
		],
		first_meet: false,
		already_called: false,
		talk_about_Lin_qui: false,
		talk_about_hair1: false,
		talk_about_hair2: false,
		talk_about_gloves: false,
		talk_about_pants: false,
		talk_about_top: false,
		talk_about_bottom: false,
		talk_about_backview: false,
		talk_about_anus: false,
		talk_about_Sonya: false,
		talk_about_Liu_Kan: false,
		start_learning: false,
		showing_panties: false,
		showing_tits: false,
		blowjob: false,
		final_blowjob: false,
		Kitana_get_amulet: false
	}),
	Raiden: new Character('Raiden', {
		id: 'Raiden',
		bdlname: 'Рейден',

		attributes: [
			'suspicion'
		],
		suspicion: 0,
		first_meet: false,
		Get_caught:false
	}),
	Shao: new Character('Shao', {
		id: 'Shao',
		bdlname: 'Шао-Кан',

		attributes: [
			'stress'
		],
		begin: false,
		stress: 0,
		souls:0
	}),
	Headvoice: {
		first_time: false,
		first_proection: false,
		first_meditation: false,
		manipulation_decision: false,
		strong_replic: false,
		talk_after_bj: false,
		talk_about_Kitana_Raiden: false,
		talk_about_amulet: false,
		talk_about_start_Kitana_corruption: false
	}
});