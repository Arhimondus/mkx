const Character = require('./Character');

class Kitana extends Character {
	constructor(data) {
		super('Kitana', data);
	}

	hairstyle(sprite) {
		this.sprite.hairstyle(sprite);
	}

	bottom(sprite) {
		this.sprite.bottom(sprite);
	}

	gloves(sprite) {
		this.sprite.gloves(sprite);
	}

	pants(sprite) {
		this.sprite.pants(sprite);
	}

	shoes(sprite) {
		this.sprite.shoes(sprite);
	}
	
	body(sprite) {
		this.sprite.body(sprite);
	}

	take_all() {
		if(this.clothes) {
			this.clothes.forEach(f => this.take(f));
		}
	}

	wear_all() {
		if(this.clothes) {
			this.clothes.forEach(f => this.wear(f));
		}
	}
}

module.exports = Kitana;