const Character = require('./Character');

class Raiden extends Character {
	constructor(data) {
		super('Raiden', data);
	}
}

module.exports = Raiden;