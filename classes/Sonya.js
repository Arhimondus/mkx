const Character = require('./Character');

class Sonya extends Character {
	constructor(data) {
		super('Sonya', data);
	}
	
	emotion(sprite) {
		this.sprite.emotion(sprite);
	}

	hairstyle(sprite) {
		this.sprite.hairstyle(sprite);
	}

	underwear_bot(sprite) {
		this.sprite.underwear_bot(sprite);
	}

	shoes(sprite) {
		this.sprite.shoes(sprite);
	}

	outerwear_bot(sprite) {
		this.sprite.outerwear_bot(sprite);
	}

	outerwear_top(sprite) {
		this.sprite.outerwear_top(sprite);
	}
}

module.exports = Sonya;