const Callable = require('./Callable'),
	BdlfContext = require('bidons-diapix/src/bdlf').default,
	Engine = require('bidons-diapix/src/base/Engine').default;

class Character extends Callable {
	// sprite: any;
	// params: String[];
	constructor(spriteName, data) {
		super();
		this.spriteName = spriteName;
		this.noParams = ['id', 'bdlname', 'attributes', 'slots', 'default', 'let', 'also'];
		this.params = [];
		for (var key in data) {
			this[key] = data[key];
			this.params.push(key);
		}
	}
	
	get sprite() {
		let children = Engine.instance.application.allScenes.find(scene => scene.constructor.name == 'Dialog').children;
		let spriteContainer = children.find(child => child.constructor.name == this.spriteName);
		return spriteContainer;
	}
	
	serialize() {	
		return this.params.filter(f => !this.noParams.includes(f)).map(p => ({ [p]: this[p] })).reduce((r,a) => ({...r, ...a}), {});
	}
	
	deserialize(data) {
		for (var key in data) {
			this[key] = data[key];
			this.params.push(key);
		}
	}
	
	getClass() {
		return this.constructor.name;
	}

	_call(data) {
		for (var key in data) {
			this.sprite[key](data[key]);
		}
	}
	
	call() {
		console.log(`${this.spriteName} call!`);
		BdlfContext.instance.sprite(this.spriteName, this.spriteName);
		this.sprite.call();
		
	}

	away() {
		console.log(`${this.spriteName} away!`);
		BdlfContext.instance.removeSprite(this.spriteName);
		this.sprite.away();
	}
	
	take() {
		for (let slot of arguments) {
			this.sprite['_' + slot].visible = false;
		}
	}

	wear() {
		for (let slot of arguments) {
			this.sprite['_' + slot].visible = true;
		}
	}
}

module.exports = Character;